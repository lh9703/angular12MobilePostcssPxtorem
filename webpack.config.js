module.exports = {
  module: {
    rules: [{
      test: /\.scss$/,
      use: [
        'postcss-loader',
        'less-loader',
      ]
    }]
  }
};